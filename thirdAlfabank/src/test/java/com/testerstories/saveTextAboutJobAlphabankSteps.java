package com.testerstories;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class saveTextAboutJobAlphabankSteps {
    WebDriver driver;
    String headerMessage;
    String infoMessage;

    @Given("^the url address of search engine$")
    public void open_url_search_engine() throws Throwable {

        driver.get("https://www.bing.com");
        assertEquals("Bing", driver.getTitle());
    }

    @When("^find alfabank site and open section about work$")
    public void find_alpha_jobs_section() throws Throwable {

        //Устанавливаем текст для поиска сайта Альфа банка
        List<WebElement> inputSearch = driver.findElements(By.id("sb_form_q"));
        inputSearch.get(0).sendKeys("альфа-банк");

        //Нажимаем кнопку поиска
        List<WebElement> buttonSearch = driver.findElements(By.id("sb_form_go"));
        buttonSearch.get(0).submit();
        waitForJSandJQueryToLoad();

        //Ожидаем загрузки последнего элемента поиска
        WebElement AlfaLinks1 = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"b_results\"]/li[7]/h2/a")));

        //Ищем сайт альфа банка (в данной реализации смотрим только первую страницу выдачи)
        List<WebElement> AlfaLinks = driver.findElements(By.cssSelector(".b_algo"));
        int iter=0;
        for (int i = 0; i < AlfaLinks.size(); i++) {
            WebElement cite = AlfaLinks.get(i).findElement(By.tagName("cite"));
            String citeLinkName = cite.getText();
            if (citeLinkName == "https://alfabank.ru") {
                iter = i;
            }
        }
        AlfaLinks.get(iter).findElement(By.tagName("a")).click();


        //Ожидаем загрузки последнего элемента поиска
        WebElement AlfaVacanciesLink = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Вакансии")));

        //Переходим по ссылке вакансии
        List<WebElement> VacanciesLink = driver.findElements(By.linkText("Вакансии"));
        VacanciesLink.get(0).click();

        //Переходим по ссылке о работе в банке
        WebElement aboutBank = driver.findElement(By.xpath("/html/body/div[1]/div/nav/nav/span[5]/a/span"));
        aboutBank.click();

        //Ожидаем загрузку текста
        WebElement aboutJobDescription = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.className("message")));
        headerMessage = aboutJobDescription.getText();

        WebElement infoJobDescription = driver.findElement(By.className("info"));
        infoMessage = infoJobDescription.getText();
    }

    @Then("^save info about work in file$")
    public void save_info_into_file() throws Throwable {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
        Date date = new Date();
        String fileName = formatter.format(date) + " Google Chrome" + " Bing.txt";
        System.out.println("fileName:" + fileName);
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        writer.write(headerMessage);
        writer.newLine();
        writer.write(infoMessage);
        writer.close();
    }

    @Before
    public void startUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\\\chromeDriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().setSize(new Dimension(1600, 1200));
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    public boolean waitForJSandJQueryToLoad() {

        WebDriverWait wait = new WebDriverWait(driver, 30);

        // wait for jQuery to load
        ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return ((Long) ((JavascriptExecutor) getDriver()).executeScript("return jQuery.active") == 0);
                } catch (Exception e) {
                    // no jQuery present
                    return true;
                }
            }
        };

        // wait for Javascript to load
        ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) getDriver()).executeScript("return document.readyState")
                        .toString().equals("complete");
            }
        };

        return wait.until(jQueryLoad) && wait.until(jsLoad);
    }

    //Получение текщего инстанса драйвера
    private Object getDriver() {
        return driver;
    }


}