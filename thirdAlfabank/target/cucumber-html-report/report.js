$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("saveTextAboutJobAlphabank.feature");
formatter.feature({
  "line": 1,
  "name": "saveTextAboutJobAlphabank",
  "description": "",
  "id": "savetextaboutjobalphabank",
  "keyword": "Feature"
});
formatter.before({
  "duration": 2104872338,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "saveTextAboutJobAlphabank",
  "description": "",
  "id": "savetextaboutjobalphabank;savetextaboutjobalphabank",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "the url address of search engine",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "find alfabank site and open section about work",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "save info about work in file",
  "keyword": "Then "
});
formatter.match({
  "location": "saveTextAboutJobAlphabankSteps.open_url_search_engine()"
});
formatter.result({
  "duration": 2826465624,
  "status": "passed"
});
formatter.match({
  "location": "saveTextAboutJobAlphabankSteps.find_alpha_jobs_section()"
});
formatter.result({
  "duration": 9326111425,
  "status": "passed"
});
formatter.match({
  "location": "saveTextAboutJobAlphabankSteps.save_info_into_file()"
});
formatter.result({
  "duration": 1475777,
  "status": "passed"
});
formatter.after({
  "duration": 1410372342,
  "status": "passed"
});
});