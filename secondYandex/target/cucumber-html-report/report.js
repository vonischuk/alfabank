$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("checkFirstSmartphone.feature");
formatter.feature({
  "line": 1,
  "name": "CheckFirstSmartphone",
  "description": "",
  "id": "checkfirstsmartphone",
  "keyword": "Feature"
});
formatter.before({
  "duration": 2495593558,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "FirstSmartphones",
  "description": "",
  "id": "checkfirstsmartphone;firstsmartphones",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "the url address and smartphones properties",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "smartphones page is loading",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "check the name of smartphones",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "headphone Beats page with credential is loading",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "check the name of headphone",
  "keyword": "Then "
});
formatter.match({
  "location": "checkFirstSmartphoneSteps.open_url_yandex()"
});
formatter.result({
  "duration": 3375239077,
  "status": "passed"
});
formatter.match({
  "location": "checkFirstSmartphoneSteps.get_page_samsung_phone()"
});
formatter.result({
  "duration": 21812021501,
  "status": "passed"
});
formatter.match({
  "location": "checkFirstSmartphoneSteps.check_equal_phones()"
});
formatter.result({
  "duration": 39321,
  "status": "passed"
});
formatter.match({
  "location": "checkFirstSmartphoneSteps.get_page_beat_headphone()"
});
formatter.result({
  "duration": 13463130147,
  "status": "passed"
});
formatter.match({
  "location": "checkFirstSmartphoneSteps.check_equal_headphones()"
});
formatter.result({
  "duration": 56217,
  "status": "passed"
});
formatter.after({
  "duration": 1461360074,
  "status": "passed"
});
});