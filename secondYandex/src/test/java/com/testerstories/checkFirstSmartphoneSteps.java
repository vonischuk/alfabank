package com.testerstories;

import static org.junit.Assert.assertEquals;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;


public class checkFirstSmartphoneSteps {
    WebDriver driver;
    String PhoneName;
    String PhoneNameOnHisPage;
    String HeadPhoneName;
    String HeadPhoneNameOnHisPage;
    private Wait<WebDriver> wait1;

    @Given("^the url address and smartphones properties$")
    public void open_url_yandex() throws Throwable {

        driver.get("https://www.yandex.com");
        assertEquals("Яндекс", driver.getTitle());
    }

    @When("^smartphones page is loading$")
    public void get_page_samsung_phone() throws Throwable {

        //Нажимаем на ссылку Маркет
        List<WebElement> marketLinks = driver.findElements(By.xpath("//*[@data-id='market']"));
        marketLinks.get(0).click();

        wait1 = new WebDriverWait(driver, 5, 1800);

        //Выбираем раздел Электроника
        List<WebElement> electronicsLinks = driver.findElements(By.xpath("//*[@class='link topmenu__link']"));
        electronicsLinks.get(0).click();

        int sleep = (int) (Math.random() * 5 * 1000);
        driver.manage().timeouts().implicitlyWait(sleep, TimeUnit.SECONDS); //protect front ya antibot
        //Выбираем подраздел Мобильные телефоны
        WebElement phoneLinks = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@data-reactid='15']")));
        phoneLinks.click();


        //Чекаем телефоны Samsung
        WebElement samsungLinks = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@data-reactid='137']")));
        samsungLinks.click();


        //Устанавливаем фильтр цены от 40000
        List<WebElement> from40000Links = driver.findElements(By.xpath("//*[@data-reactid='41']"));
        from40000Links.get(0).sendKeys("40000");

        //Ждем когда запустится ajax и ждем когда он отработает
        Thread.sleep(1000);
        waitForJSandJQueryToLoad();

        //Ожидаем загрузку данных и забираем имя первого элемента
        WebElement FirstPhoneName = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div/div[4]/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[3]/div[2]/a")));
        PhoneName = FirstPhoneName.getText();

        //Переходим по первому элементу из таблицы телефонов
        List<WebElement> FirstPhoneLinks = driver.findElements(By.xpath("/html/body/div/div[4]/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[3]/div[2]/a"));
        FirstPhoneLinks.get(0).click();

        //Ожидаем загрузку страницы первого телефона и забираем имя из заголовка
        WebElement PhoneNameOnHisPageWE = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='title title_size_28 title_bold_yes']")));
        PhoneNameOnHisPage = PhoneNameOnHisPageWE.getText();


    }

    @Then("^check the name of smartphones$")
    public void check_equal_phones() throws Throwable {
        assertEquals(PhoneName, PhoneNameOnHisPage);
    }

    @Then("^check the name of headphone$")
    public void check_equal_headphones() throws Throwable {
        assertEquals(HeadPhoneName, HeadPhoneNameOnHisPage);
    }

    @Before
    public void startUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\\\chromeDriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().setSize(new Dimension(1600, 1200));
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    public boolean waitForJSandJQueryToLoad() {

        WebDriverWait wait = new WebDriverWait(driver, 30);

        // wait for jQuery to load
        ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return ((Long) ((JavascriptExecutor) getDriver()).executeScript("return jQuery.active") == 0);
                } catch (Exception e) {
                    // no jQuery present
                    return true;
                }
            }
        };

        // wait for Javascript to load
        ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) getDriver()).executeScript("return document.readyState")
                        .toString().equals("complete");
            }
        };

        return wait.until(jQueryLoad) && wait.until(jsLoad);
    }

    //Получение текщего инстанса драйвера
    private Object getDriver() {
        return driver;
    }

    @When("^headphone Beats page with credential is loading$")
    public void get_page_beat_headphone() throws Throwable {

        //Выбираем раздел Электроника
        List<WebElement> electronicsLinks = driver.findElements(By.xpath("//*[@class='link topmenu__link']"));
        electronicsLinks.get(0).click();

        //Выбираем подраздел Наушники
        WebElement headphoneLinks = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(
                        By.xpath("//*[@data-reactid='26']")));
        headphoneLinks.click();

        //Чекаем наушники Beats
        WebElement beatsHeadphonesLinks = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@data-reactid='80']")));
        beatsHeadphonesLinks .click();


        //Устанавливаем фильтр цены от 17000 до 25000
        List<WebElement> from17000Links = driver.findElements(By.xpath("//*[@data-reactid='40']"));
        from17000Links.get(0).sendKeys("17000");
        List<WebElement> from25000Links = driver.findElements(By.xpath("//*[@data-reactid='45']"));
        from25000Links.get(0).sendKeys("25000");


        //Ждем когда запустится ajax и ждем когда он отработает
        Thread.sleep(1000);
        waitForJSandJQueryToLoad();

        //Ожидаем загрузку данных и забираем имя первого элемента
        WebElement FirstHeadPhoneName = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div/div[4]/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[3]/div[2]/a")));
        HeadPhoneName = FirstHeadPhoneName.getText();

        //Переходим по первому элементу из таблицы наушников
        List<WebElement> FirstHeadPhoneLinks = driver.findElements(By.xpath("/html/body/div/div[4]/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[3]/div[2]/a"));
        FirstHeadPhoneLinks.get(0).click();

        //Ожидаем загрузку страницы первого наушника и забираем имя из заголовка
        WebElement HeadPhoneNameOnHisPageWE = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='title title_size_28 title_bold_yes']")));
        HeadPhoneNameOnHisPage = HeadPhoneNameOnHisPageWE.getText();

    }

}