package com.company;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {

        String fileName = args[0];

        File file = new File(fileName);

        if (!file.exists()) {
            throw new Exception("По указанному пути файл не существует");
        }

        String value = Files.lines(Paths.get(fileName)).reduce("", String::concat);


        //Разбиваем на массив строк
        String[] arrayOfString = value.split(",");


        //Преобразовываем в int и фильтруем по значениям 0-20
        ArrayList<Integer> resultArray = new ArrayList<>();
        for (int i = 0; i < arrayOfString.length; i++) {
            try {
                int digit = Integer.parseInt(arrayOfString[i]);
                if (digit > -1 && digit < 21) {
                    resultArray.add(digit);
                }
            } catch (Throwable t) {
            }
        }
        Collections.sort(resultArray);
        System.out.println("По возрастанию: " + resultArray);
        Collections.reverse(resultArray);
        System.out.println("По убыванию:" + resultArray);


    }
}
